---
date: 2019-07-13T09:00:00+00:00
title: Dokumentasi
---

## Dokumentasi Manjaro-X

***
### 1. Spesifikasi

* Bootable DVD atau Flasdisk minimal 4 GB.
* Memiliki 2 GHz dual-core prosesor atau lebih.
* Memiliki RAM minimal 2 GB (Disarankan 4 GB).
* Memiliki Penyimpanan data lebih dari 25 GB.
* Memiliki Resolusi layar minimal 1366 x 768 px.

***
### 2. Persiapan Penting

Pengguna Windows:

* **Manjaro-X tidak dianjurkan untuk Dual-OS dengan Sistem Operasi Windows**.
* **Cadangkan data penting Anda ke hardisk eksternal, apabila data anda masih menggunakan NTFS.**
* **Cadangkan data Anda mulai dari direktori C:/Users/$NAMAUSER.**

***
Pengguna GNU/Linux:

* Jika Anda bermigrasi ke Manjaro-X pastikan Anda mempunyai partisi **/home**, Jika tidak punya maka cadangkan data USER Anda ke hardisk eksternal.

***
### 3. Cara membuat Bootable

Ketika membuat Bootable, cadangkan semua data penting Anda di dalam USB Flasdisk. Sebab perangkat lunak bootable akan menghapus seluruh data. Lakukanlah pencadangan ini jika ada data yang penting.

Kemudian buatlah bootable sesuai sistem operasi Anda:

* _**[Cara membuat Bootable di GNU/Linux](/dokumentasi/gnulinux)**_

* _**[Cara membuat Bootable di Windows](/dokumentasi/windows)**_

***
### 4. Mengatur UEFI/LEGACY

Aturlah boot manager anda dengan **USB Drives** sebagai boot order yang pertama. Secara umum menu Bios menggunakan tombol **Esc atau F2** sedangkan untuk boot order menggunakan **F9, F10, atau F12**. Kemudian carilah informasi BIOS Anda telah mendukung **UEFI** atau tidak, jika tidak maka BIOS-nya masih menggunakan **LEGACY**.

***
### 5. Cara memasang Manjaro-X

* Ketika sudah memasuki Live USB, pastikan semua hardware Anda bekerja, seperti Wifi, Bluetooth (Laptop), Touchpad, Kecerahan layar (Laptop), dan Suara.
* Pilih Menu **Install Manjaro-X** dengan menekan tombol Super. Tunggulah beberapa saat hingga muncul **Manjaro-X Installer**.
* Jika Anda menggunakan Laptop pastikan adaptor pengisi daya Anda telah tertancap dan Koneksi Internet Anda sedang mati.
* Pada tab **Welcome**, Gantilah pengaturan bahasa **American English** Menjadi **Indonesia**. kemudian klik **Berikutnya**.
* Pada tab **Lokasi**, Gantilah Wilayah **Asia** dan Zona **Jakarta**. Zona ini tergantung dari daerah Anda berada. Kemudian klik **Berikutnya**.
* Pada tab **Papan Ketik**, Gantilah dari Indonesia Jawi ke **English (US)** dengan jenis **Default**. Jenis papan ketik ini tergantung papan ketik yang Anda gunakan. Kemudian klik **Berikutnya**.
* Pada tab **Partisi**, Pilihlah **Pemartisian manual**
    - Pilih **Partisi Baru Tabel**
    - Pilih **MBR** jika sistem bios Anda LEGACY, Pilih **GPT** jika sistem bios Anda UEFI.
    - **1. Jika menggunakan LEGACY**
        - Klik **membuat**, Ukuran: 512 Mib, Sistem berkas:ext4, Titik kait: /boot, Tanda: boot.
        - Klik **membuat**, Ukuran: 4096 Mib, Sistem berkas:linuxswap, Titik kait: -, Tanda: swap.
        - Klik **membuat**, Ukuran: 51200 Mib, Sistem berkas:ext4, Titik kait: /.
        - Klik **membuat**, Ukuran: (sisanya) Mib, Sistem berkas:ext4, Titik kait: /home.
    - **2. Jika menggunakan UEFI**
        - Klik **membuat**, Ukuran: 512 Mib, Sistem berkas:ext4, Titik kait: /boot/efi, Tanda: boot.
        - Klik **membuat**, Ukuran: 512 Mib, Sistem berkas:ext4, Titik kait: /boot.
        - Klik **membuat**, Ukuran: 4096 Mib, Sistem berkas:linuxswap, Titik kait: -, Tanda: swap.
        - Klik **membuat**, Ukuran: 51200 Mib, Sistem berkas:ext4, Titik kait: /.
        - Klik **membuat**, Ukuran: (sisanya) Mib, Sistem berkas:ext4, Titik kait: /home.
    - Kemudian klik **Berikutnya**.
* Pada tab **Pengguna**, Isilah data Anda.
    - Siapakah nama Anda: Isilah nama Lengkap anda. Misalnya "John Doe"
    - Nama apa yang anda gunakan untuk login: Isilah nama tanpa spasi dan gunakan huruf kecil. Misalnya "johndoe"
    - Apakah nama komputer ini: Isilah tanpa spasi dan gunakan huruf kecil. Misalnya "manjaro"
    - Pilih sebuah kata sandi: isilah kata sandi Anda
    - Ceklis, Gunakan sandi yang sama untuk akun administrator.
    - Kemudian klik **Berikutnya**.
* Pada tab **Ikhtisar**, klik **install**, kemudian akan muncul dialog baru dan klik **install sekarang**.
* Proses pemasangan Manjaro-X sedang berjalan.
* Pada tab **Selesai**, klik **Kelar**. Kemudian komputer akan menyalakan ulang.
* Kemudian Cabut Flasdisknya ketika komputer dalam keadaan mati.
* Selesai.

***
### 6. Pilihan Opsional.

* Anda bisa merubah lagi **Boot Order** di menu Bios dengan urutan HDD/SSD sebagai urutan pertama.
* Anda bisa menghapus memformat kembali Bootable Flasdisk menjadi Flasdisk Normal kembali dengan cara _**[ seperti ini](/dokumentasi/format-flasdisk.md)**_.

***

Catatan:

* 512 Mib = 0,5 GB
* 51200 Mib = 50 GB
* Cuplikan dokumentasi menyusul.
