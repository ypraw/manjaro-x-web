---
date: 2019-07-13T09:00:00+00:00
title: Tim Pengembang
---

## Tim Pengembang

![hervyqa-manjaro-x](hervyqa-manjaro-x.webp)

### Hervy Qurrotul Ainur Rozi (Developer & co-Maintener Web)

Destop yang digunakan adalah GNOME, perilis ISO versi terbaru, mengelola OSDN server, mengatur paket dan depedensi. Disisi lain sebagai Desainer dan Ilustrator yang menggunakan FLOSS (Free/Libre Open Source Software). Juga sebagai kontributor di openSUSE, openSUSE ID, LibreOffice ID, GNOME ID, dan Creative Common ID.

Kontak:

* Telegram : **[@hervyqa](https://t.me/hervyqa)**
* Twitter : **[@hervyqa](https://twitter.com/hervyqa)**
* Instagram : **[@hervyqa](https://instagram.com/hervyqa)**
* Gitlab : **[hervyqa](https://gitlab.com/hervyqa)**
* Gitlab GNOME : **[hervyqa](https://gitlab.gnome.org/hervyqa)**
* Github : **[hervyqa](https://github.com/hervyqa)**
* Surel : **[hervyqa@gmail.com](mailto:hervyqa@gmail.com)**
* Laman : **[https://hervyqa.com](https://hervyqa.com)**

***

