---
date: 2019-07-13T09:00:00+00:00
title: Fitur
---

## Mengapa Manjaro-X?

**Manjaro-X** merupakan pemasang Manjaro Linux yang lengkap untuk orang awam, desainer, pengajar, animator, dan pengembang aplikasi GTK. Setelah memasang Manjaro-X versi rilis sistem operasi akan kembali menggunakan Manjaro linux. Sebab Manjaro-X bukanlah distribusi dengan branding setelah dipasang, melainkan sistem operasi Manjaro Linux yang di bundel kembali untuk kepraktisan penggunaan.

Tujuan Manjaro-X ini hanyalah untuk kepentingan pendidikan GNU/Linux semata, agar tercapainya kemudahan dan kenyamanan bagi pengguna yang pertama kali melihat sistem operasi GNU/Linux. Tujuan lainnya Manjaro-X ini untuk memperkaya jenis distribusi Manjaro Linux tek resmi sebagai sistem operasi GNU/Linux yang sederhana, kuat, dan elegan.

Di Manjaro-X beberapa tema dan gambar dinding khas Manjaro dihilangkan sehingga tampilan Manjaro-X lebih bersih dan natural. Dengan demikian akan mengembalikan _cita rasa_ GNOME itu sendiri.

### Kernel LTS

Manjaro-X mulai versi 18.1.2 akan tetap menggunakan kernel LTS. Dikatakan LTS karena Manjaro-X menggunakan **linux-lts** didalam depedensinya, tak hanya itu Manjaro-X juga memberikan dukungan untuk kartu wifi broadcom LTS, dan  dukungan modul Headers LTS.

Manjaro-X juga lebih stabil karena masih mendukung perangkat lama, ada dukungan modul untuk catalist dan nvidia. Pada versi 18 ini Manjaro-X menghapus **manjaro-settings-manager** sebab sistem operasi GNU/Linux ini dibuat untuk orang awam sehingga tidak perlu memperdulikan kernel yang terbarukan, jika ada kernel LTS terbaru maka secara otomatis sistem akan mendeteksi kernel baru ketika diperbarui.

### Terpasang Encoding Suara dan Film

Manjaro-X sudah memasukkan depedensi codec suara dan film sehingga Anda bisa memutarnya dengan begitu mudah, cukup klik dua kali berkas tersebut kemudian langsung memutar.

### Tema Gelap dan Terang

Tema gelap dari Adwaita-Dark secara baku. Anda bisa mengganti mode cerah dengan menekan perintah **Alt+F2** kemudian mengetik **lightmode**, atau kembali ke mode petang dengan mengetik **darkmode**.
 
Mode malam aktif secara baku. Fitur ini berlaku untuk jam sore hingga terbit fajar. Mode malam akan mengurangi cahaya biru sehingga mata tetap aman dan nyaman.

### GNOME Aplikasi

GNOME Aplikasi sudah tersedia aplikasi untuk mutitasking, mulai dari penjelajah web, pembuka berkas, pemutar musik dan film, pembakar disk, membuat cuplikan, pembuka gambar, pembuka fonta, pembuka surel, kalender, akun daring, dan masih banyak lagi.

### Perangkat Lunak Perkantoran

LibreOffice merupakan perangkat lunak perkantoran yang dikembangkan oleh OpenOffice.org oleh organisasi The Document Foundation dan tersedia untuk platform GNU/Linux, Windows, dan Mac. LibreOffice memiliki program pengolah kata, pengolah angka, dan pengolah salindia presentasi. Program ini lebih dikenal sebagai Writer, Calc, dan Impress.

LibreOffice menggunakan format dokumen berstandar ISO yaitu Open Document Format (ODF). LibreOffice Writer merupakan pengolah kata untuk membuat proposal, makalah, skripsi, thesis, disertasi dan dokumen lain. LibreOffice juga dijadikan sebagai program perkantoran baku di Manjaro-X.

### Pencetak EPSON

Manjaro-X terdapat dukungan pencetak EPSON yang terdiri dari 669 jenis printer. Dukungan Pencetak ini diambil dari paket AUR **epson-inkjet-printer-escpr** dan **epson-inkjet-printer-escpr2**.
[lihat daftar pencetak](https://gitlab.com/manjaro-x/manjaro-x-profil/raw/master/manjaro-x-epson-list.txt)

### Dukungan MHWD

Secara baku Manjaro-X akan mendeteksi perangkat keras dengan maksimal seperti dukungan Intel, AMD GPU, Nvidia, Broadcom, Realtek, dan Qualcomm. Sehingga akan mendukung beberapa perangkat keras yang sebagian tidak bebas.

### AUR Aktif

Secara baku Manjaro-X mengaktifkan AUR, sehingga pencarian aplikasi lebih luas selain mengambil paket dari repositori resmi.

### Roling Rilis

Kelebihan Roling rilis ini akan mempermudah _Upgrade_. Tidak perlu ada pesta rilis, tidak perlu memasang kembali sistem operasi ketika ada versi baru. Cukup _Upgrade_ dengan GUI, begitu mudah bukan.

## Gabung di kanal!

Bergabunglah di kanal telegram Manjaro-X di **[t.me/manjaro_x](https://t.me/manjaro_x)**.
