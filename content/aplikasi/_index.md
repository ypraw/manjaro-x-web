---
date: 2019-05-14T09:00:00+00:00
title: Aplikasi
---

## Aplikasi GUI

### Blender

![blender](blender.svg)

Pengolah animasi dan sebagai Video Editor yang handal.

### Notes (bijiben)

![bijiben](bijiben.svg)

Aplikasi ringan untuk menulis catatan

### Brasero

![brasero](brasero.svg)

Pembakar diska CD/DVD

### Disk Utility (baobab)

![baobab](baobab.svg)

Melihat ruang kosong dan jumlah berkas dan direktori.

### Kamera (cheese)

![cheese](cheese.svg)

Foto dan merekam video dengan WebCam.

### Culwer

![curlew](curlew.svg)

Perangkat lunak perubah encoding audio dan video.

### Dconf-editor

![dconf-editor](dconf-editor.svg)

Merubah pengaturan GNOME dengan lanjutan.

### Backup (deja-dup)

![deja-dup](deja-dup.svg)

Mencadangkan data secara daring dan luring.

### Drawing

![drawing](drawing.svg)

Perangkat untuk membuat sketsa dengan cepat.

### Empathy

![empathy](empathy.svg)

Aplikasi layanan obrolan dengan framework telephaty.

### Web (epiphany)

![epiphany](epiphany.svg)

Peramban Web baku di GNOME.

### PDF Viewer (evince)

![evince](evince.svg)

Pembuka PDF.

### Evolution

![evolution](evolution.svg)

Membuka surat elektronik.

### Feedreader

![feedreader](feedreader.svg)

Aplikasi RSS klien.

### Fragments

![fragments](fragments.svg)

Klien Bittorent.

### Color Picker (gcolor3)

![gcolor3](gcolor3.svg)

Pengambil warna.

### Gedit

![gedit](gedit.svg)

Teks editor GNOME.

### Giggle

![giggle](giggle.svg)

Membuka Git dengan GUI.

### Gimp

![gimp](gimp.svg)

Aplikasi pengolah bitmap.

### Glade

![glade](glade.svg)

Mendesain UI aplikasi GNOME.

### Boxes (gnome-boxes)

![gnome-boxes](gnome-boxes.svg)

Mesin Virtualisasi dan jarak jauh yang sederhana.

### Break Timer (gnome-break-timer)

![gnome-break-timer](gnome-break-timer.svg)

Digunakan untuk pewaktu istirahat.

### Builder (gnome-builder)

![gnome-builder](gnome-builder.svg)

Membangun aplikasi GTK.

### Calendar (gnome-calendar)

![gnome-calendar](gnome-calendar.svg)

Kalendar dari GNOME.

### Clocks (gnome-clocks)

![gnome-clocks](gnome-clocks.svg)

Jam dari GNOME.

### Contacts (gnome-contacts)

![gnome-contacts](gnome-contacts.svg)

Kontak dari GNOME.

### Characters (gnome-characters)

![gnome-characters](gnome-characters.svg)
Aplikasi pemetaan karakter dari GNOME.

### Disks (gnome-disk-utility)

![gnome-disk-utility](gnome-disk-utility.svg)
Pengelolaan diska.

### Fonts (gnome-font-viewer)

![gnome-font-viewer](gnome-font-viewer.svg)
Pembuka fonta dari GNOME

### GNOME LaTex (gnome-latex)

![gnome-latex](gnome-latex.svg)
Penyunting Latex dari GNOME

### Maps (gnome-maps)

![gnome-maps](gnome-maps.svg)
Pembuka peta dari GNOME

### MultiWriter (gnome-multi-writer)

![gnome-multi-writer](gnome-multi-writer.svg)
Membuat bootable diska dari GNOME.

### Net tool (gnome-nettool)

![gnome-nettool](gnome-nettool.svg)
Membuka jaringan secara GUI.

### Gnome-Pie

![gnome-Pie](gnome-pie.svg)
Pembuka menu aplikasi dengan indah.

### Podcast (gnome-podcasts)

![gnome-podcasts](gnome-podcasts.svg)
Aplikasi Podcast dari GNOME.

### Screenshot (gnome-screenshot)

![gnome-screenshot](gnome-screenshot.svg)
Membuat cuplikan layar.

### Sound Recorder (gnome-sound-recorder)

![gnome-sound-recorder](gnome-sound-recorder.svg)
Perekam suara dari GNOME.

### System Monitor (gnome-system-monitor)

![gnome-system-monitor](gnome-system-monitor.svg)
Melacak penggunaan daya CPU, Memori, Swap, dan Jaringan.

### Terminal (gnome-terminal)

![gnome-terminal](gnome-terminal.svg)
Terminal Emulator dari GNOME.

### To Do (gnome-todo)

![gnome-todo](gnome-todo.svg)
Pengelolaan tugas dari GNOME.

### Tweaks (gnome-tweaks)

![gnome-tweaks](gnome-tweaks.svg)
Mengoprek pengaturan GNOME lebih lanjut.

### Weather (gnome-weather)

![gnome-weather](gnome-weather.svg)
Mengakses kondisi cuaca saat ini.

### gThumb

![gthumb](gthumb.svg)
Penampil gambar.

### Translation Editor (gtranslator)

![gtranslator](gtranslator.svg)
Editor dan penerjemah berkas PO.

### Handbrake

![handbrake](handbrake.svg)
Video transcoder.

### Inkscape

![inkscape](inkscape.svg)
Aplikasi pengolah vector.

### Klavaro

![klavaro](klavaro.svg)
Program belajar mengetik.

### LibreOffice Base (libreoffice-still)

![libreoffice-base](libreoffice-base.svg)
Aplikasi Perkantoran untuk penyimpanan database.

### LibreOffice Calc (libreoffice-still)

![libreoffice-calc](libreoffice-calc.svg)
Aplikasi Perkantoran untuk perhitungan dan pelaporan.

### LibreOffice Impress (libreoffice-still)

![libreoffice-impress](libreoffice-impress.svg)
Aplikasi Perkantoran untuk presentasi.

### LibreOffice Writer (libreoffice-still)

![libreoffice-writer](libreoffice-writer.svg)
Aplikasi Perkantoran untuk teks dokumen.

### Meld

![meld](meld.svg)
Pembanding berkas dan direktori.
ayar untuk individu tunanetra.

### Peek

![peek](peek.svg)
Perekam layar GIF.

### Passwords and Key (seahorse)

![seahorse](seahorse.svg)
Penyimpan kata sandi dan kunci.

### Simple-scan

![simple-scan](simple-scan.svg)
Utilitas scanner.

### Sound Juicer (sound-juicer)

![sound-juicer](sound-juicer.svg)
Pengekstrak CD dari GNOME.

### Rhythmbox

![rhythmbox](rhythmbox.svg)
Pemutar Musik dari GNOME.

### Telegram Desktop (telegram-desktop)

![telegram-desktop](telegram-desktop.svg)
Klien Telegram destop resmi.

### Video (totem)

![totem](totem.svg)
Pemutar Video dari GNOME.

### Vinagre

![vinagre](vinagre.svg)
Penampil destop jarak jauh dengan layanan VNC server.


## Aplikasi CLI

### hugo

Generator web statis dari Go.

### youtube-dl

Perintah untuk mengunduh Video.

### tor

Penjelajah jaringan dengan anonim.

### manjaro-tools

Pengembang untuk Manjaro ISO
